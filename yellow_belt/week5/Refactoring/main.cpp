#include <iostream>
#include <string>
#include <vector>

#define DEVELOP
#define OK 0

using namespace std;

class Person
{
public:
    Person(const string& name, const string& type) : Name(name), Type(type) {}

    void Logger() const
    {
        cout << Type << ": " << Name;
    }

    virtual void Walk(const string& destination) const
    {
        Logger();
        cout << " walks to: " << destination << endl;
    }

    const string Name, Type;

};

class Student : public Person
{
public:

    Student(const string& name, const string& favouriteSong) : Person(name, "Student"), FavouriteSong(favouriteSong) {}

    void Learn() const
    {
        Logger();
        cout << " learns" << endl;
    }

    virtual void Walk(const string& destination) const override
    {
        Person::Walk(destination);
        SingSong();
    }

    void SingSong() const
    {
        Logger();
        cout << " sings a song: " << FavouriteSong << endl;
    }

private:

    const string FavouriteSong;

};


class Teacher : public Person
{
public:

    Teacher(const string& name, const string& subject) : Person(name, "Teacher"), Subject(subject) {}

    void Teach() const
    {
        Logger();
        cout << " teaches: " << Subject << endl;
    }

private:

    const string Subject;

};


class Policeman : public Person
{
public:

    Policeman(const string& name) : Person(name, "Policeman") {}

    void Check(const Person& t) const
    {
        Logger();
        cout << " checks " << t.Type << ". " << t.Type << "'s name is: " << t.Name << endl;
    }

};


void VisitPlaces(const Person& t, const vector<string>& places)
{
    for (auto p : places)
    {
        t.Walk(p);
    }
}

#ifdef DEVELOP
int main() {
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");

    VisitPlaces(t, {"Moscow", "London"});
    p.Check(s);
    VisitPlaces(s, {"Moscow", "London"});

    return OK;
}
#endif
