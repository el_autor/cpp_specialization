#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

class Person {
public:
	Person(const string& type, const string& name) : Type(type), Name(name) {}

	string GetType() const {
		return Type;
	}

	string GetTitle() const {
		stringstream ss;
		ss << Type << ": " << Name;
		return ss.str();
	}

	virtual void Walk(const string& destination) {
		cout << GetTitle() << " walks to: " << destination << endl;
	}


	virtual ~Person() {}

	const string Type;
	const string Name;

};

class Student : public Person {
public:

	Student(const string& name, const string& favouriteSong) : Person("Student", name), FavouriteSong(favouriteSong) {
	}

	void Learn() {
		cout << GetTitle() << " learns" << endl;
	}

	void Walk(const string& destination) override {
		Person::Walk(destination);
		SingSong();
	}

	void SingSong() {
		cout << GetTitle() << " sings a song: " << FavouriteSong << endl;
	}

public:
	const string FavouriteSong;
};


class Teacher : public Person {
public:

	Teacher(const string& name, const string& subject) : Person("Teacher", name), Subject(subject) {
	}

	void Teach() {
		cout << GetTitle() << " teaches: " << Subject << endl;
	}

public:
	const string Subject;
};


class Policeman : public Person {
public:
	Policeman(const string& name) : Person("Policeman", name) {}

	void Check(const Person& person) {
		cout << GetTitle() << " checks " << person.GetType() << ". " << person.GetType() << "'s name is: " << person.Name << endl;
	}

};


void VisitPlaces(Person& person, const vector<string> places) {
	for (auto p : places) {
		person.Walk(p);
	}
}


int main() {
	Teacher t("Jim", "Math");
	Student s("Ann", "We will rock you");
	Policeman p("Bob");

	VisitPlaces(t, {"Moscow", "London"});
	p.Check(s);
	VisitPlaces(s, {"Moscow", "London"});
	return 0;
}
