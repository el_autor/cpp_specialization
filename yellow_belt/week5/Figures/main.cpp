#include <iostream>
#include <vector>
#include <memory>
#include <iomanip>
#include <cmath>

//#define DEVELOP

using namespace std;

class Figure
{
public:

    virtual string Name() = 0;
    virtual float Perimeter() = 0;
    virtual float Area() = 0;

};

class Circle : public Figure
{
public:

    Circle(const int& r)
    {
        _r_ = r;
    }

    virtual string Name() override
    {
        return "CIRCLE";
    }

    virtual float Perimeter() override
    {
        return 2 * 3.14 * _r_; 
    }

    virtual float Area() override
    {
        return 3.14 * _r_ * _r_;
    }

private:

    int _r_;

};

class Rect : public Figure
{
public:

    Rect(const int& a, const int& b)
    {
        _a_ = a;
        _b_ = b;
    }

    virtual string Name() override
    {
        return "RECT";
    }

    virtual float Perimeter() override
    {
        return 2 * (_a_ + _b_);
    }

    virtual float Area() override
    {
        return _a_ * _b_;
    }

private:

    int _a_, _b_;

};

class Triangle : public Figure
{
public:

    Triangle(const int& a, const int& b, const int& c)
    {
        _a_ = a;
        _b_ = b;
        _c_ = c;
    }

    virtual string Name() override
    {
        return "TRIANGLE";
    }

    virtual float Perimeter() override
    {
        return _a_ + _b_ + _c_;
    }

    virtual float Area() override
    {
        float half_perimeter = Perimeter() / 2;

        return sqrt(half_perimeter * (half_perimeter - _a_) * (half_perimeter - _b_) * (half_perimeter - _c_));
    }

private:

    int _a_, _b_, _c_;

};

shared_ptr<Figure> CreateFigure(istringstream& stream)
{
    string type = "";

    stream >> type;

    if (type == "TRIANGLE")
    {
        int a = 0, b = 0 ,c = 0;

        stream >> a >> b >> c;

        return make_shared<Triangle>(a, b, c);
    }
    else if (type == "RECT")
    {
        int a = 0, b = 0;

        stream >> a >> b;

        return make_shared<Rect>(a, b); 
    }
    else
    {
        int r = 0;

        stream >> r;

        return make_shared<Circle>(r);
    }
}

#ifdef DEVELOP
int main()
{
    vector<shared_ptr<Figure>> figures;

    for (string line; getline(cin, line); )
    {
        istringstream is(line);

        string command;
        is >> command;

        if (command == "ADD")
        {
            figures.push_back(CreateFigure(is));
        }
        else if (command == "PRINT")
        {
            for (const auto& current_figure : figures)
            {
                cout << fixed << setprecision(3)
                << current_figure->Name() << " "
                << current_figure->Perimeter() << " "
                << current_figure->Area() << endl;
            }
        }
    }

    return 0;
}
#endif