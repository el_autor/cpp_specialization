#include <iostream>

#define OK 0
//#define DEVELOP

using namespace std;

class Animal
{
public:
    Animal(const string& init_string) : Name(init_string) {}
    const string Name;    
};

class Dog : public Animal
{
public:
    Dog(const string& name) : Animal(name) {}

    void Bark()
    {
        cout << Name << " barks: woof!" << endl;  
    }
};

#ifdef DEVELOP
int main()
{
    Dog doggy("doggy");
    doggy.Bark();

    return OK;
}
#endif