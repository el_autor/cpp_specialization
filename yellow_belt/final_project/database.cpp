#include "database.h"

ostream& operator<<(ostream& stream, const pair<Date, string>& data)
{
    stream << data.first << " " << data.second;

    return stream;
}

ostream& operator<<(ostream& stream, const pair<Date, vector<string>>& date)
{
	for (const string& event : date.second)
	{
		stream << date.first << " " << event << endl;
	}

	return stream;
}

bool operator<(const pair<Date, string>& left, const pair<Date, string>& right)
{
	return left.first < right.first;
}


bool operator==(const pair<Date, string>& left, const pair<Date, string>& right)
{
	return left.first == right.first && right.second == left.second;
}

void Database::Add(const Date& date, const string& event)
{
    unordered_set<string>& unique_events = uniq[date];

    if (unique_events.find(event) == end(unique_events))
    {
        unique_events.insert(event);
        all_data[date].push_back(event);
    }
}

void Database::Print(ostream& stream) const
{
    for (const pair<Date, vector<string>>& date : all_data)
    {
        for (const string& event : date.second)
        {
            stream << date.first << " " << event << endl;
        }
    }
}

int Database::RemoveIf(function<bool(const Date&, const string&)> predicate)
{
	int result = 0;

	map<Date, vector<string>> new_all_data;
	map<Date, unordered_set<string>> new_uniq;

    for (pair<const Date, vector<string>>& date : all_data)
    {
        const auto border = stable_partition(date.second.begin(), date.second.end(), 
			                                [predicate, date](const auto& item)
                                            {
			                                    return predicate(date.first, item);
		                                    });

		const size_t tmp = date.second.size();

        if (border == date.second.end())
        {
			result += tmp;
        }
        else
        {
			new_all_data[date.first] = vector<string>(border, date.second.end());
			new_uniq[date.first] = unordered_set<string>(border, date.second.end());

			result += tmp - new_all_data.at(date.first).size();
        }
    }

	all_data = new_all_data;
	uniq = new_uniq;

	return result;
}

vector<pair<Date, string>> Database::FindIf(function<bool(const Date&, const string&)> predicate) const
{
    vector<pair<Date, string>> founded;
    map<Date, vector<string>> tmp;

    for (const pair<Date, vector<string>>& date : all_data)
    {
        vector<string> tempVector;

        copy_if(begin(date.second), end(date.second), back_inserter(tempVector), [predicate, date](const auto& value)
                                                                                 {
                                                                                     return predicate(date.first, value);
                                                                                 });

        if (tempVector.size() != 0)
        {
            tmp[date.first] = tempVector;
        }
    }

    for (const pair<Date, vector<string>>& date : tmp)
    {
        for (const string& event : date.second)
        {
            founded.push_back(make_pair(date.first, event));
        }
    }  

    return founded;
}


pair<Date, string> Database::Last(const Date& date) const
{ 
    map<Date, vector<string>>::const_iterator it = all_data.upper_bound(date);
   
    if (all_data.size() == 0 || it == begin(all_data))
    {
        throw invalid_argument("");
    }
    else
    {
        it--;
    }
       
    return make_pair(it->first, it->second[it->second.size() - 1]);
}