#include "date.h"

#define SEPARATOR '-'

using namespace std;

Date::Date(const int& year, const int& month, const int& day)
{
    _year_ = year;
    _month_ = month;
    _day_ = day;
}

int Date::get_year() const
{
    return _year_;
}

int Date::get_month() const
{
    return _month_;
}

int Date::get_day() const
{
    return _day_;
}

Date ParseDate(istream& stream)
{
    string date = string(10, '0'), number_in_string_form = "";
    int year = 0, month = 0, day = 0;
    char border = 0;

    stream >> year;
    stream >> border;
    stream >> month;
    stream >> border;
    stream >> day; 

    Date new_date = Date(year, month, day);

    return new_date;
}

ostream& operator<<(ostream& stream, const Date& date)
{
    stream << setw(4) << setfill('0') << date.get_year() << "-" <<
              setw(2) << setfill('0') << date.get_month() << "-" <<
              setw(2) << setfill('0') << date.get_day();

    return stream;
}

bool operator==(const Date& date_1, const Date& date_2)
{
    if (date_1.get_day() == date_2.get_day() &&\
        date_1.get_month() == date_2.get_month() &&\
        date_1.get_year() == date_2.get_year())
    {
        return true;
    }

    return false;
}

bool operator<(const Date& date_1, const Date& date_2)
{
    if (date_1.get_year() < date_2.get_year())
    {
        return true;
    } 
    else if (date_1.get_year() == date_2.get_year() && date_1.get_month() < date_2.get_month())
    {
        return true;
    }
    else if (date_1.get_year() == date_2.get_year() && date_1.get_month() == date_2.get_month() && date_1.get_day() < date_2.get_day())
    {
        return true;
    }

    return false;  
}

bool operator>(const Date& date_1, const Date& date_2)
{
    if (date_1.get_year() > date_2.get_year())
    {
        return true;
    } 
    else if (date_1.get_year() == date_2.get_year() && date_1.get_month() > date_2.get_month())
    {
        return true;
    }
    else if (date_1.get_year() == date_2.get_year() && date_1.get_month() == date_2.get_month() && date_1.get_day() > date_2.get_day())
    {
        return true;
    }

    return false;  
}
