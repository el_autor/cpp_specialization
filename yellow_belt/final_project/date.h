#pragma once

#include <string>
#include <iostream>
#include <set>
#include <iomanip>

using namespace std;

class Date
{
public:

    Date(const int& year, const int& month, const int& day);
    int get_year() const;
    int get_month() const;
    int get_day() const;

private:

    int _year_, _month_, _day_;
};

Date ParseDate(istream& stream);
ostream& operator<<(ostream& stream, const Date& date);
bool operator==(const Date& date_1, const Date& date_2);
bool operator<(const Date& date_1, const Date& date_2);
bool operator>(const Date& date_1, const Date& date_2);