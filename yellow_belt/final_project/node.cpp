#include "node.h"

EmptyNode::EmptyNode(){}

bool EmptyNode::Evaluate(const Date& date, const string& event) const
{
    return true;
}

DateComparisonNode::DateComparisonNode(const Comparison& type, const Date& date) : _type_(type), _date_(date){}

bool DateComparisonNode::Evaluate(const Date& date, const string& event) const
{
    switch (this->_type_)
    {
        case (Comparison::Less):
            return (date < this->_date_);
        
        case (Comparison::Greater):
            return (date > this->_date_);

        case (Comparison::LessOrEqual):
            return !(date > this->_date_);

        case (Comparison::GreaterOrEqual):
            return !(date < this->_date_);

        case (Comparison::Equal):
            return (date == this->_date_);

        default:
            return !(date == this->_date_);
    }
}

EventComparisonNode::EventComparisonNode(const Comparison& type, const string& event) : _type_(type), _event_(event){}

bool EventComparisonNode::Evaluate(const Date& date, const string& event) const
{
    switch (this->_type_)
    {
        case (Comparison::Less):
            return (event < this->_event_);
        
        case (Comparison::Greater):
            return (event > this->_event_);

        case (Comparison::LessOrEqual):
            return !(event > this->_event_);

        case (Comparison::GreaterOrEqual):
            return !(event < this->_event_);
        
        case (Comparison::Equal):
            return (event == this->_event_);
        
        default:
            return !(event == this->_event_);
    }
}

LogicalOperationNode::LogicalOperationNode(const LogicalOperation& operation, shared_ptr<Node> left, shared_ptr<Node> right) : _operation_(operation), _left_(left), _right_(right){}

bool LogicalOperationNode::Evaluate(const Date& date, const string& event) const
{
    if (this->_operation_ == LogicalOperation::And)
    {
        return (_left_->Evaluate(date, event) && _right_->Evaluate(date, event)); 
    }
    else
    {
        return (_left_->Evaluate(date, event) || _right_->Evaluate(date, event));
    }
}