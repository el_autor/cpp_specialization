#pragma once
#include <vector>
#include <string>
#include <list>
#include <algorithm>
#include <map>
#include <unordered_set>
#include <functional>
#include <iostream>
#include "date.h"

using namespace std;


class Database
{
public:

	void Add(const Date& date, const string& event);
	void Print(ostream& out) const;
	int RemoveIf(function<bool(const Date&, const string&)> predicate);
    vector<pair<Date, string>> FindIf(function<bool(const Date&, const string&)> predicate) const;
	pair<Date, string> Last(const Date& date) const;

private:

	map<Date, vector<string>> all_data;
	map<Date, unordered_set<string>> uniq;

};

ostream& operator<<(ostream& os, const pair<Date, vector<string>>& pair_);
bool operator<(const pair<Date, string>& left, const pair<Date, string>& right);
bool operator==(const pair<Date, string>& left, const pair<Date, string>& right);
ostream& operator<<(ostream& os, const pair<Date, string>& pair_);