#pragma once

#include <memory>
#include "date.h"

enum Comparison
{
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual,
    Equal,
    NotEqual,
};

enum LogicalOperation
{
    Or,
    And,
};

struct Node
{
    virtual bool Evaluate(const Date& date, const string& event) const = 0;
};

class EmptyNode : public Node
{
public:

    EmptyNode();
    bool Evaluate(const Date& date, const string& event) const override;

};

class DateComparisonNode : public Node
{
public:

    DateComparisonNode(const Comparison& type, const Date& date);
    bool Evaluate(const Date& date, const string& event) const override;

private:

    const Comparison _type_;
    const Date _date_;

};

class EventComparisonNode : public Node
{
public:

    EventComparisonNode(const Comparison& type, const string& event);
    bool Evaluate(const Date& date, const string& event) const override;

private:

    const Comparison _type_;
    const string _event_;

};

class LogicalOperationNode : public Node
{
public:

    LogicalOperationNode(const LogicalOperation& operation, shared_ptr<Node> left, shared_ptr<Node> right);
    bool Evaluate(const Date& date, const string& event) const override;

private:

    const LogicalOperation _operation_;
    shared_ptr<Node> _left_, _right_;

};