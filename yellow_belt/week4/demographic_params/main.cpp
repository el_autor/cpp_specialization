#include <iostream>
#include <vector>
#include <algorithm>

#ifdef DEVELOP
using namespace std;

enum class Gender
{
    FEMALE,
    MALE
};

struct Person
{
    int age;
    Gender gender;
    bool is_employed;
};

template <typename InputIt>
int ComputeMedianAge(InputIt range_begin, InputIt range_end) {
  if (range_begin == range_end) {
    return 0;
  }
  vector<typename InputIt::value_type> range_copy(range_begin, range_end);
  auto middle = begin(range_copy) + range_copy.size() / 2;
  nth_element(
      begin(range_copy), middle, end(range_copy),
      [](const Person& lhs, const Person& rhs) {
        return lhs.age < rhs.age;
      }
  );
  return middle->age;
}
#endif

static int predicate_female(struct Person& person)
{
    return (person.gender == Gender::FEMALE);
}

static int predicate_male(struct Person& person)
{
    return (person.gender == Gender::MALE);
}

static int predicate_employed_female(struct Person& person)
{
    return (person.gender == Gender::FEMALE && person.is_employed);
}

static int predicate_unemployed_female(struct Person& person)
{
    return (person.gender == Gender::FEMALE && !person.is_employed);
}

static int predicate_employed_male(struct Person& person)
{
    return (person.gender == Gender::MALE && person.is_employed);
}

static int predicate_unemployed_male(struct Person& person)
{
    return (person.gender == Gender::MALE && !person.is_employed);
}

void PrintStats(vector<Person> persons)
{
    vector<Person>::iterator it;

    cout << "Median age = " << ComputeMedianAge(begin(persons), end(persons)) << endl;

    it = partition(begin(persons), end(persons), predicate_female);
    cout << "Median age for females = " << ComputeMedianAge(begin(persons), it) << endl;

    it = partition(begin(persons), end(persons), predicate_male);
    cout << "Median age for males = " << ComputeMedianAge(begin(persons), it) << endl;

    it = partition(begin(persons), end(persons), predicate_employed_female);
    cout << "Median age for employed females = " << ComputeMedianAge(begin(persons), it) << endl;

    it = partition(begin(persons), end(persons), predicate_unemployed_female);
    cout << "Median age for unemployed females = " << ComputeMedianAge(begin(persons), it) << endl;

    it = partition(begin(persons), end(persons), predicate_employed_male);
    cout << "Median age for employed males = " << ComputeMedianAge(begin(persons), it) << endl;

    it = partition(begin(persons), end(persons), predicate_unemployed_male);
    cout << "Median age for unemployed males = " << ComputeMedianAge(begin(persons), it) << endl;
}

#ifdef DEVELOP
int main() {
  vector<Person> persons = {
      {31, Gender::MALE, false},
      {40, Gender::FEMALE, true},
      {24, Gender::MALE, true},
      {20, Gender::FEMALE, true},
      {80, Gender::FEMALE, false},
      {78, Gender::MALE, false},
      {10, Gender::FEMALE, false},
      {55, Gender::MALE, true},
  };
  PrintStats(persons);
  return 0;
}
#endif