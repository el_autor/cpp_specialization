#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>

#define DEVELOP

using namespace std;

template <typename RandomIt>
pair<RandomIt, RandomIt> FindStartsWith(RandomIt range_begin, RandomIt range_end, const string& prefix)
{
    pair<RandomIt, RandomIt> interval;

    auto it = partition_point(range_begin, range_end, [prefix](const string& line)
    {
        if (line.length() < prefix.length())
        {
            return line < prefix;
        }
        else
        {
            string local;

            for (unsigned int i = 0; i < prefix.length(); i++)
            {
                local.push_back(line[i]);
            }

            return local < prefix;
        }
    }); 

    interval.first = it;

    it = partition_point(it, range_end, [prefix](const string& line)
    {
        if (line.length() < prefix.length())
        {
            return false;
        }
        else
        {
            string local;

            for (unsigned int i = 0; i < prefix.length(); i++)
            {
                local.push_back(line[i]);
            }

            return local == prefix;
        }
    });

    interval.second = it;
    return interval;
}


#ifdef DEVELOP
int main()
{
    const vector<string> sorted_strings = {"moscow", "motovilikha", "murmansk"};
    const auto mo_result = FindStartsWith(begin(sorted_strings), end(sorted_strings), "mo");

    for (auto it = mo_result.first; it != mo_result.second; ++it)
    {
        cout << *it << " ";
    }
    cout << endl;
  
    const auto mt_result =
    FindStartsWith(begin(sorted_strings), end(sorted_strings), "mt");
    cout << (mt_result.first - begin(sorted_strings)) << " " << (mt_result.second - begin(sorted_strings)) << endl;
  
    const auto na_result = FindStartsWith(begin(sorted_strings), end(sorted_strings), "na");
    cout << (na_result.first - begin(sorted_strings)) << " " << (na_result.second - begin(sorted_strings)) << endl;
  
    return 0;
}
#endif