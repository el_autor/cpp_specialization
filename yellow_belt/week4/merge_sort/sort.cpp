#include <iostream>
#include <vector>
#include <algorithm>

//#define DEVELOP
//#define MY_SORT 
//#define PARTS_2
#define PARTS_3

using namespace std;

#ifdef MY_SORT
template<typename RandomIt>
void merge(vector<typename RandomIt::value_type> source, RandomIt dest_begin, RandomIt dest_end)
{
    /*
    int i_part_1 = 0, i_part_2 = (dest_end - dest_begin) / 2;
    cout << i_part_1 << " " << i_part_2 << endl;
    */
    RandomIt copy_begin = dest_begin;

    /*
    while (dest_begin != dest_end)
    {
        cout << "bad" << source[i_part_1] << " " << source[i_part_2] << " " << endl; 
        if (i_part_1 < dest_end - copy_begin && source[i_part_1] <= source[i_part_2])
        {
            *dest_begin = source[i_part_1];
            i_part_1++;
        }
        else if (i_part_2 < dest_end - copy_begin)
        {
            *dest_begin = source[i_part_2];
            i_part_2++;
        }

        dest_begin++;
    }
    */

    for (int i = 0; i < dest_end - dest_begin; i++)
    {
        *dest_begin = source[i];
        dest_begin++; 
    }

    sort(copy_begin, dest_end);

    /*
    cout << "here" << endl;
    for (RandomIt i = copy_begin; i < dest_end; i++)
    {
        cout << *i << " ";
    }
    cout << "bango" << endl;
    */
}   
#endif

#ifdef PARTS_2
template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    if (range_end - range_begin < 2)
    {
        return;
    }

    vector<typename RandomIt::value_type> copy_vector(range_begin, range_end);
    MergeSort(begin(copy_vector), begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 2);
    MergeSort(begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 2, end(copy_vector));

    //merge(copy_vector, range_begin, range_end);

    merge(begin(copy_vector), begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 2,\
                begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 2, end(copy_vector),\
                range_begin);
}
#endif

#ifdef PARTS_3
template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    if (range_end - range_begin < 2)
    {
        return;
    }

    vector<typename RandomIt::value_type> copy_vector(range_begin, range_end);
    vector<typename RandomIt::value_type> local;
    MergeSort(begin(copy_vector), begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 3);
    MergeSort(begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 3, begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 3 * 2);
    MergeSort(begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 3 * 2, end(copy_vector));

    merge(begin(copy_vector), begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 3,\
          begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 3, begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 3 * 2,\
          back_inserter(local));
    merge(begin(local), end(local),\
          begin(copy_vector) + (end(copy_vector) - begin(copy_vector)) / 3 * 2, end(copy_vector),\
          range_begin);
}
#endif

#ifdef DEVELOP
int main() {
    vector<int> v = { 6, 4, 7, 6, 4, 4, 0, 1, 3 };

    MergeSort(begin(v), end(v));
    for (int x : v)

    {
        cout << x << " ";
    }
    return 0;

    cout << endl;
}
#endif