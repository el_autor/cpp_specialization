#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

#define OK 0

using namespace std;

static int factorial(const int& a)
{
    int init = 1;

    for (int i = 2; i <= a; i++)
    {
        init *= i;
    }

    return init;
}

static void show_permutation(vector<int>& data)
{
    for (vector<int>::iterator i = begin(data); i < end(data); i++)
    {
        cout << *i << " ";
    }

    cout << endl;
}

int main()
{
    vector<int> data;
    int a = 0, total_permutations = 0;

    cin >> a;
    total_permutations = factorial(a);

    for (int i = a; i > 0; i--)
    {
        data.push_back(i);
    }

    show_permutation(data);

    for (int i = 1; i < total_permutations; i++)
    {
        prev_permutation(begin(data), end(data));
        show_permutation(data);
    }

    return OK;
}