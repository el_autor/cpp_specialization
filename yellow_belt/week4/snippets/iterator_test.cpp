#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <iterator>
#include <numeric>

#define OK 0

using namespace std;

typedef vector<int>::iterator it_vector_t;

template <typename it>
void show_vector(it start, it end)
{
    for (it i = start; i < end; i++)
    {
        cout << *i << " ";
    }

    cout << endl;
}

int main()
{
    set<int> s = { 1, 2, 3, 4, 5 };
    vector<int> v;

    // Task 1
    // Вычленить элементы в вектор v, которые не удовлетворяют условию

    /*
    v.assign(begin(s) , end(s));
    show_vector(begin(v), end(v));
    auto it = partition(begin(v), end(v), [](const int& value)
    {
        return value >= 3;
    });
    v.erase(it, end(v));
    show_vector(begin(v), end(v));
    */
    // не подходит

    /*
    vector<int> v_2;
    partition_copy(begin(s), end(s), back_inserter(v), back_inserter(v_2), [](const int& value)
    {
        return value >= 3;
    });
    show_vector(begin(v), end(v));
    // не подходит
    */

    /*
    copy_if(begin(s), end(s), back_inserter(v), [](const int& value)
    {
        return value < 3;
    });
    show_vector(begin(v), end(v));
    // подходит
    */

    /*
    remove_copy_if(begin(s), end(s), back_inserter(v), [](const int& value)
    {
        return value >= 3;
    });
    show_vector(begin(v), end(v));
    // подходит
    */

    /*
    auto it = partition(begin(s), end(s), [](const int& value)
    {
        return value >= 3;
    });
    copy(it, end(s), back_inserter(v));
    show_vector(begin(v), end(v));
    // тут ошибка
    */

    /*
    v.assign(begin(s), end(s));
    auto it = remove_if(begin(v), end(v), [](const int& value)
    {
        return value < 3;
    });
    v.erase(it, end(v));
    show_vector(begin(v), end(v));
    // не подходит
    */

    /*
    v.assign(begin(s), end(s));
    auto it = remove_if(begin(v), end(v), [](const int& value)
    {
        return value >= 3;
    });
    v.erase(it, end(v));
    show_vector(begin(v), end(v));
    // подходит
    */

    /*
    v.assign(begin(s), end(s));
    auto it = partition(begin(v), end(v), [](const int& value)
    {
        return value >= 3;
    });
    v.erase(begin(v), it);
    show_vector(begin(v), end(v));
    // подходит
    */
    
    /*
    copy_if(begin(s), end(s), back_inserter(v), [](const int& value)
    {
        return value >= 3;
    });
    show_vector(begin(v), end(v));
    // не подходит
    */

    /*
    v.assign(begin(s), end(s));
    sort(begin(v), end(v), [](const int& left, const int& right)
    {
        return left > right;
    });
    auto it = partition_point(begin(v), end(v), [](const int& value)
    {
        return value >= 3;
    }); 
    v.erase(begin(v), it);
    show_vector(begin(v), end(v));
    // подходит
    */

    /*
    auto it = remove_if(begin(s), end(s), [](const int& value)
    {
        return value < 3;
    });
    copy(it, end(s), back_inserter(v));
    show_vector(begin(v), end(v));
    // не подходит
    */

    // Task 2
    vector<int> vec;
    auto vector_begin = begin(vec);
    auto vector_end = end(vec);

    string str;
    auto string_begin = begin(str);
    auto string_end = end(str);

    set<int> set;
    auto set_begin = begin(set);
    auto set_end = end(set);

    //auto res = is_permutation(set_begin, set_end, vector_begin); // компилится
    //partial_sum(set_begin, set_end, back_inserter(vec)); // компилится
    //partial_sum(set_begin, set_end, vector_begin); // компилится
    //auto res = is_permutation(set_begin, set_end, back_inserter(vec)); // не компилится
    //auto res = is_heap(set_begin ,set_end); // не компилится
    //partial_sum(vector_begin, vector_end, set_begin); // не компилится
    //auto res = next_permutation(string_begin, string_end); // компилится
    //auto res = is_permutation(vector_begin, vector_end, vector_begin); // компилитс
    //auto res = next_permutation(set_begin, set_end); // не компилится
    //auto res = accumulate(vector_begin, vector_end, 0); // компилится
    //auto res = is_heap(string_begin, string_end); // компилится

    // Task 3
    /*
    vector<int> v_3 = { 1, 2, 3, 8, 7, 6 };

    auto it = is_sorted_until(rbegin(v_3), rend(v_3));
    show_vector(rbegin(v_3), it);
    */

    return OK;
}