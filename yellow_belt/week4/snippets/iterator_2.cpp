#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Lang 
{
    string name;
    int age;
};

int main()
{
    vector<struct Lang> langs = { { "Python", 1 }, { "C++", 2}, { "C", 3 }, { "Java", 4 }, { "C#", 5 } };

    auto result = find_if(begin(langs), end(langs), [](struct Lang& lang)
    {
        return lang.name[0] == 'C';
    });

    cout << result->name << " " << result->age << endl; 

    return 0;
}

// Выводит C++
