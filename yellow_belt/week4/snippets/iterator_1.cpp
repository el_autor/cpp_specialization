#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    vector<string> langs = { "Python", "C++", "C", "Java", "C#" };

    auto result = find_if(begin(langs), end(langs), [](const string& lang)
    {
        return lang[0] == 'C';
    });

    cout << *(result) << endl;

    return 0;
}

// Выводит C++
