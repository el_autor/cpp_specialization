#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

#define OK 0

using namespace std;

template <typename RandomIt>
pair<RandomIt, RandomIt> FindSegment(RandomIt range_begin, RandomIt range_end, int left, int right)
{
    // return { next(lower_bound(range_begin, range_end, left - 1)), prev(upper_bound(range_begin, range_end, right + 1)) }; 
    // no
    // return { lower_bound(range_begin, range_end, left), lower_bound(range_begin, range_end, right) };
    // no
    // return { lower_bound(range_begin, range_end, left), upper_bound(range_begin, range_end, right) };
    // yes
    return { upper_bound(range_begin, range_end, left - 1), lower_bound(range_begin, range_end, right + 1) };
    // yes
}

int main()
{
    vector<int> v = { 1, 2, 2, 3, 3, 3, 4, 11, 12, 12, 25, 123 };
    pair<vector<int>::iterator, vector<int>::iterator> segment = FindSegment(begin(v), end(v), 3, 12);

    for (auto it = segment.first; it < segment.second; it++)
    {
        cout << *it << " ";
    }

    return OK;
}