#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <algorithm>

#define DEVELOP

using namespace std;

class Person
{
public:
    void ChangeFirstName(int year, const string& first_name)
    {
        if (!data.count(year))
        {
            data[year].second = "unknown";
        }
        
        data[year].first = first_name;
    }

    void ChangeLastName(int year, const string& last_name)
    {
        if (!data.count(year))
        {
            data[year].first = "unknown";
        }

        data[year].second = last_name;
    }

    string GetFullName(int year)
    {
        auto it = data.upper_bound(year);
        vector<string> situation = { "unknown", "unknown" };

        if (it == begin(data))
        {
            return "Incognito";
        }
        else
        {
            it--;

            situation[0] = it->second.first;
            situation[1] = it->second.second;

            if (situation[0] == "unknown")
            {
                while (it != begin(data) && it->second.first == "unknown")
                {
                    it--;
                }

                situation[0] = it->second.first;
            }
            else if (situation[1] == "unknown")
            {
                while (it != begin(data) && it->second.second == "unknown")
                {
                    it--;
                }

                situation[1] = it->second.second;
            }   
        }


        if (situation[0] != "unknown" && situation[1] != "unknown")
        {
            return situation[0] + " " + situation[1];
        }
        else if (situation[0] != "unknown" && situation[1] == "unknown")
        {
            return situation[0] + " with unknown last name";
        }
        else if (situation[1] != "unknown" && situation[0] == "unknown")
        {
            return situation[1] + " with unknown first name";
        }
        else
        {
            return "Incognito";
        }
    }

private:
    map<int, pair<string, string>> data;

};

#ifdef DEVELOP
int main()
{
    Person person;

    person.ChangeFirstName(1965, "Polina");
    person.ChangeLastName(1967, "Sergeeva");
    for (int year : {1900, 1965, 1990})
    {
        cout << person.GetFullName(year) << endl;
    }
  
    person.ChangeFirstName(1970, "Appolinaria");
    for (int year : {1969, 1970})
    {
        cout << person.GetFullName(year) << endl;
    }
  
    person.ChangeLastName(1968, "Volkova");
    for (int year : {1969, 1970}) 
    {
        cout << person.GetFullName(year) << endl;
    }
  
    return 0;
}
#endif 