#include <iostream>
#include <deque>

#define OK 0

using namespace std;

int main()
{
    int number = 0, operations = 0, operand = 0;
    deque<string> expression;
    string operation, prev_operation;
    prev_operation = "*";

    cin >> number >> operations;
    expression.push_back(to_string(number));

    for (int i = 0; i < operations; i++)
    {
        cin >> operation >> operand;

        if ((operation == "*" || operation == "/") && (prev_operation == "-" || prev_operation == "+"))
        {
            expression.push_front("(");
            expression.push_back(") ");
        }
        else
        {
            expression.push_back(" ");
        }

        expression.push_back(operation);
        expression.push_back(" " + to_string(operand));
        prev_operation = operation;
    }

    for (auto it = begin(expression); it < end(expression); it++)
    {
        cout << *it;
    }

    return OK;
}