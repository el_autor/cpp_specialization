#include <vector>
#include <algorithm>

/*
template <typename data_t>
void RemoveDuplicates(vector<data_t>& elems)
{   
    for (auto i = begin(elems); i < end(elems) - 1; i++)
    {
        auto it = remove(i + 1, end(elems), *i);

        if (it != end(elems))
        {
            elems.erase(it, end(elems));
        }   
    }
}
*/

template <typename data_t>
void RemoveDuplicates(vector<data_t>& elems)
{
    sort(begin(elems), end(elems));
    auto it = unique(begin(elems), end(elems));
    elems.erase(it, end(elems));
}
