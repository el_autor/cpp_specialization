#include <iostream>
#include <vector>

#define OK 0

using namespace std;
#include "remover.h"

int main()
{
    vector<int> data = { 1, 1, 2, 1, 3, 1, 4, 2, 2, 3, 9 };

    RemoveDuplicates(data);

    for (vector<int>::iterator i = begin(data); i < end(data); i++)
    {
        cout << *i << " ";
    }

    cout << endl;

    return OK;
}