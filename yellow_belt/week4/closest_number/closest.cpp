#include <iostream>
#include <set>

#define DEVELOP

using namespace std;

set<int>::const_iterator FindNearestElement(const set<int>& data, int border)
{   
    set<int>::const_iterator it_before, it_after;

    if (begin(data) == end(data))
    {
        return end(data);
    }

    it_after = data.lower_bound(border);

    if (it_after == begin(data))
    {
        return it_after;
    }

    it_before = prev(it_after);

    if (it_after == end(data))
    {
        return it_before;
    }

    if (abs(*it_after - border) < abs(*it_before - border))
    {
        return it_after;
    }
    else if (abs(*it_after - border) > abs(*it_before - border))
    {
        return it_before;
    }

    return it_before;
}

#ifdef DEVELOP
int main()
{
  set<int> numbers = {1, 4, 6};
  cout <<
      *FindNearestElement(numbers, 0) << " " <<
      *FindNearestElement(numbers, 3) << " " <<
      *FindNearestElement(numbers, 5) << " " <<
      *FindNearestElement(numbers, 6) << " " <<
      *FindNearestElement(numbers, 100) << endl;
      
  set<int> empty_set;
  
  cout << (FindNearestElement(empty_set, 8) == end(empty_set)) << endl;
  return 0;
}
#endif